# AngularCodeTesting

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 10.0.5.

## Mock Json Server

Run `npm run mock:server` in terminal first that will navigate to `http://localhost:3000` and then open second new terminal - Run `ng serve`.

## Please run to start the Angular application + Json Server with the following cmd in terminal

Run `npm run start:proxy:mock:server` and navigate to `http://localhost:4200`.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.


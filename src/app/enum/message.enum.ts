export enum Message {
    SAVE_SUCCESS = 'Save Success!',
    SAVE_FAIL = 'Save Fail!',
    UPDATE_SUCCESS = 'Update Success!',
    UPDATE_FAIL = 'Update Fail!',
    DATA_NOT_FOUND = 'Data Not Found!',
    SOMETHING_WRONG = 'Something was wrong!',
    DELETE_SUCCESS = 'Delete Success!',

    EXIST_USER = 'The Contact already exists.'
}
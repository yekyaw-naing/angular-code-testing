import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { MatDialog } from '@angular/material';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Message } from 'src/app/enum/message.enum';
import { ContactsService } from 'src/app/_services/contacts.service';
import { ContactCreateComponent } from '../contact-create/contact-create.component';

@Component({
  selector: 'app-contact-list',
  templateUrl: './contact-list.component.html',
  styleUrls: ['./contact-list.component.css']
})
export class ContactListComponent implements OnInit {

  dataList = [];
  searchValue: string;
  comfirmDeleteId: string;
  isLoading = false;
  isServerRun = false;

  page = 1;
  pageLimit = 5;
  totalCount: number;

  @ViewChild('closeAddExpenseModal') closeAddExpenseModal: ElementRef;

  constructor(
    private contactService: ContactsService,
    private toastr: ToastrService,
    public dialog: MatDialog,
    private spinner: NgxSpinnerService) {

  }

  ngOnInit(): void {
    // this.getAllList(); 
    this.getTotalCountList();   // get total count of all data(total Contacts count) 
    this.getAllListByPaging(1, this.pageLimit);
  }

  /* retrieve all data without pagination */
  getAllList() {
    this.spinner.show();
    this.contactService.getAll().subscribe(data => {
      this.spinner.hide();
      if (data && data.length > 0) {
        this.isLoading = false;
        this.dataList = data;
      } else {
        this.isLoading = true;
        this.dataList = [];
      }
    });
  }

  /* retrieve data with server-side pagination */
  getAllListByPaging(page: any, limit: any) {
    this.spinner.show();
      this.contactService.getAllPaging(page, limit).subscribe(data => {
        this.spinner.hide();
        if (data && data.length > 0) {
          this.isLoading = false;
          this.dataList = data;
        } else {
          this.isLoading = true;
          this.dataList = [];
        }
      }, (error) => {
        this.spinner.hide();
        this.dataList = [];
        // Error callback
        console.error('error caught in component');
        console.log(error);
        this.isServerRun = true;
      }
      );
  }

  /* get page totalCount */
  getTotalCountList() {
    this.contactService.getAll().subscribe(data => {
      if (data && data.length > 0) {
        this.totalCount = data.length;
      };
    })
  }

  /* store data to comfirm delete function(deleteById) */
  storeDeleteId(sid: string) {
    this.comfirmDeleteId = sid;
  }

  deleteById(id: string) {
    this.contactService.deleteById(id).subscribe(data => {
      if (data.id == null) {
        this.toastr.success(Message.DELETE_SUCCESS);
      } else {
        this.toastr.error(Message.SOMETHING_WRONG);
      }
      this.closeAddExpenseModal.nativeElement.click();
      // this.getAllList();
      this.getAllListByPaging(1, this.pageLimit);
      this.page = 1;
      this.getTotalCountList();   // get total count of all data(total Contacts count) 
    });
  }

  /*  call and open dialog box of contact-create Component */
  openDialog() {
    const dialogRef = this.dialog.open(ContactCreateComponent, {
      width: '1000px',
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      // this.getAllList();
      this.getAllListByPaging(1, this.pageLimit);
      this.page = 1;
      this.getTotalCountList();   // get total count of all data(total Contacts count) 
    });
  }

  /* (with ID) call and open dialog box of contact-create Component */
  openDialogByID(id: string) {
    const dialogRef = this.dialog.open(ContactCreateComponent, {
      width: '1000px',
      data: id
    });
    dialogRef.afterClosed().subscribe(result => {
      console.log(`Dialog result: ${result}`);
      // this.getAllList();
      this.getAllListByPaging(1, this.pageLimit);
      this.page = 1;
      this.getTotalCountList();   // get total count of all data(total Contacts count) 
    });
  }

  /*   For Pagination - paging */
  pageChanged(e: number) {
    this.page = e;
    this.getAllListByPaging(e, this.pageLimit);
  }

  search() {
    if (this.searchValue && this.searchValue.trim().length > 0) {
      this.searchList(this.searchValue);
    } else {
      this.getAllList();
    }
  }

  searchList(searchVal: string) {
    this.spinner.show();
    this.contactService.search(searchVal).subscribe(data => {
      this.spinner.hide();
      if (data.length > 0) {
        this.dataList = data;
      } else {
        this.dataList = [];
        this.toastr.warning(Message.DATA_NOT_FOUND);
      }
    });
  }

}

import { Component, Inject, OnInit, Optional } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { ActivatedRoute, Router } from '@angular/router';
import { NgxSpinnerService } from 'ngx-spinner';
import { ToastrService } from 'ngx-toastr';
import { Message } from 'src/app/enum/message.enum';
import { ContactsService } from 'src/app/_services/contacts.service';

@Component({
  selector: 'app-contact-create',
  templateUrl: './contact-create.component.html',
  styleUrls: ['./contact-create.component.css']
})
export class ContactCreateComponent implements OnInit {

  contactForm!: FormGroup;
  model = { id: '', name: '', email: '', phone: '' };

  isEmailExist = false;
  isPhoneExist = false;

  constructor(
    private dialog: MatDialog,
    public dialogRef: MatDialogRef<ContactCreateComponent>,       // catch the passing data(ID) from Contact-list Component
    @Optional() @Inject(MAT_DIALOG_DATA) public passDataId: any,
    private contactService: ContactsService,
    private formBuilder: FormBuilder,
    private toastr: ToastrService,
    private router: Router,
    private route: ActivatedRoute,
    private spinner: NgxSpinnerService,) {
    this.createForm();
  }

  ngOnInit(): void {
    if (this.passDataId) {
      this.getById(this.passDataId);
    }
  }

  createForm() {
    this.contactForm = this.formBuilder.group({
      id: [''],
      name: ['', Validators.required],
      email: ['', Validators.required],
      phone: ['', Validators.required],
    });
  }

  getById(id: string) {
    this.contactService.getById(id).subscribe(data => {
      if (data !== null && data !== undefined) {
        this.model.id = data.id;
        this.model.name = data.name;
        this.model.email = data.email;
        this.model.phone = data.phone;
      }
    });
  }

  onSubmit() {
    this.isEmailExist = false;
    this.isPhoneExist = false;
    /* OnSubmit - Update Contact */
    if (this.model.id) {
      this.updateContact();
    }
    /* OnSubmit - Save Contact */
    else {
      this.createContact();
    }
  }

  updateContact() {
    this.spinner.show();
    /* Can Update if (name, email, phone) are same */
    this.contactService.updateById(this.model.id, this.contactForm.value).subscribe(response => {
      this.spinner.hide();
      if (response) {
        this.toastr.success(Message.UPDATE_SUCCESS);
        this.dialog.closeAll();
      } else {
        this.toastr.error(Message.UPDATE_FAIL);
        this.dialog.closeAll();
      }
    });

    /* Filtering with Email and Phone(to check contact exist)*/
    /* this.contactService.filterEmail(this.contactForm.get('email').value).subscribe(data => {
      this.spinner.hide();
      if (data.length > 0) {
        this.isEmailExist = true;
      } else {
        this.isEmailExist = false;
        this.contactService.filterPhone(this.contactForm.get('phone').value).subscribe(data => {
          if (data.length > 0) {
            this.isPhoneExist = true;
          } else {
            this.isPhoneExist = false;
            this.contactService.updateById(this.model.id, this.contactForm.value).subscribe(response => {
              this.spinner.hide();
              if (response) {
                this.toastr.success(Message.UPDATE_SUCCESS);
                this.dialog.closeAll();
              } else {
                this.toastr.error(Message.UPDATE_FAIL);
                this.dialog.closeAll();
              }
            });
          }
        });
      }
    }); */
  }

  createContact() {
    this.spinner.show();
    /* Filtering with Email and Phone(to check contact exist)*/
    this.contactService.filterEmail(this.contactForm.get('email').value).subscribe(data => {
      this.spinner.hide();
      if (data.length > 0) {
        this.isEmailExist = true;
      } else {
        this.isEmailExist = false;
        this.contactService.filterPhone(this.contactForm.get('phone').value).subscribe(data => {
          if (data.length > 0) {
            this.isPhoneExist = true;
          } else {
            this.isPhoneExist = false;
            this.contactService.create(this.contactForm.value).subscribe(data => {
              if (data) {
                this.toastr.success(Message.SAVE_SUCCESS);
                this.clearData();
                this.dialog.closeAll();
              } else {
                this.toastr.error(Message.SAVE_FAIL);
                this.dialog.closeAll();
              }
            });
          }
        });
      }
    });
  }

  clearData() {
    this.contactForm.reset();
  }

}

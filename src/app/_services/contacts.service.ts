import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

const apiUrl = `${environment.apiUrl}/contacts`

@Injectable({
  providedIn: 'root'
})
export class ContactsService {

  reqHeader: any;

  constructor(private http: HttpClient) { }

  getAllPaging(page: string, limit: string) {
    const params = new HttpParams()
      .set('_page', page)
      .set('_limit', limit);
    return this.http.get<any>(`${apiUrl}`, { headers: this.reqHeader, params });
  }

  getAll() {
    return this.http.get<any>(`${apiUrl}`);
  }

  getById(id: string) {
    return this.http.get<any>(`${apiUrl}/${id}`)
      .pipe(
        catchError(this.handleError<any>('get by id'))
      );
  }

  create(contact: any) {
    return this.http.post<any>(`${apiUrl}`, contact).pipe(
      catchError(this.handleError<any>('create contact'))
    );
  }

  updateById(id: string, contact: any) {
    return this.http.put<any>(`${apiUrl}/${id}`, contact).pipe(
      catchError(this.handleError<any>('update contact'))
    );
  }

  deleteById(id: string) {
    return this.http.delete(`${apiUrl}/${id}`).pipe(
      catchError(this.handleError<any>('delete contact'))
    );
  }

  search(searchVal: string) {
    return this.http.get<[]>(`${apiUrl}?q=${searchVal}`);
  }

  filterEmail(email: string) {
    return this.http.get<any>(`${apiUrl}?email=${email}`);
  }

  filterPhone(phone: string) {
    return this.http.get<any>(`${apiUrl}?phone=${phone}`);
  }

  filterEmailPhone(email:string, phone:string) {
    return this.http.get<any>(`${apiUrl}?email=${email}&phone=${phone}`);
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      // console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      console.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }
}

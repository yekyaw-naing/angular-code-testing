import { NgModule } from '@angular/core';
import { Routes, RouterModule, PreloadAllModules } from '@angular/router';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'contacts' },
    {
        path: 'contacts',
        loadChildren: () => import('./components/contacts/contacts.module').then(m => m.ContactsModule)
    },
    { path: '**', redirectTo: ''}
];

@NgModule({
    declarations: [],
    imports: [
        RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })    
    ],
    exports: [RouterModule]
})
export class AppRoutingModule { }